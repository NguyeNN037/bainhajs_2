function tinhTongKySo() {
    // nhận giá trị của số 2 chữ số
    var soChinhEl = Number(document.getElementById("number").value);
    // Tách và lấy chữ số hàng đơn vị và hàng chục
    var soDonVi = parseInt(soChinhEl % 10);
    var soHangChuc = parseInt((soChinhEl/10))%10;
    // Lấy tổng
    var sum = soDonVi + soHangChuc;
    var contentResult = `<p>Tổng của 2 ký số là:</p>
                        ${sum}`;
    
    document.getElementById("result_5").innerHTML = contentResult;
}